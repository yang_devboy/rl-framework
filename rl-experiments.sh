#!/bin/sh
#
# Force Bourne Shell if not Sun Grid Engine default shell (you never know!)
#
#$ -S /bin/sh
#
# I know I have a directory here so I'll use it as my initial working directory
#
#$ -wd /vol/grid-solar/sgeusers/yuyang2 
#
# Mail me at the b(eginning) and e(nd) of the job
#
#$ -M yang.yu@ecs.vuw.ac.nz
#$ -m be
#
# End of the setup directives
#
# Stdout from programs and shell echos will go into the file
#    scriptname.o$JOB_ID
# Set up the Java environment
#
if [ -z "$SGE_ARCH" ]; then
   echo "Can't determine SGE ARCH"
else
   if [ "$SGE_ARCH" = "lx-amd64" ]; then
       JAVA_HOME="/usr/pkg/java/sun-7"
   fi
fi

if [ -z "$JAVA_HOME" ]; then
   echo "Can't define a JAVA_HOME"
else
   export JAVA_HOME
   PATH="/usr/pkg/java/bin:${JAVA_HOME}/bin:${PATH}"; export PATH
fi
#
# Now let's do something useful, but first change into the job-specific directory that should
#  have been created for us
# Then copy something we already know exists into it
#
# Check we have somewhere to work now and if we don't, exit nicely.
#  We could do more to try and run here but this is just a test
#
if [ -d /local/tmp/yuyang2/$JOB_ID ]; then
        cd /local/tmp/yuyang2/$JOB_ID
else
        echo "There's no job directory to change into "
        echo "Exiting"
        exit 1
fi
#
# Now we are in the job-specific directory so
#
# Copy the input file to the local directory
#
# Here the mountain car environment is used as an example.
# You need to change it to the corresponding jar execution command for different experiments
cp -R /vol/grid-solar/sgeusers/yuyang2/rl-framework .
{
   java -jar rl-framework/environments/mountain-car/mountaincar-environment.jar random 8888
} &
echo "The environment has been initialized..."
./rl-framework/agent_handler_in_grid.py
#
# Now we move the output to a place to pick it up from later
#  (really should check that directory exists too, but this is just a test)
#
mkdir -p /vol/grid-solar/sgeusers/yuyang2/$JOB_ID
cp rl-framework/*.log  /vol/grid-solar/sgeusers/yuyang2/$JOB_ID
#
echo "==================The experiments have been done================================="