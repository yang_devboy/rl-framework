#!/usr/bin/python2
# Echo client program
import socket
import sys
#from numpy import * #import random
import agents.RandomAgent.Agent
#import agents.PolBack
#import agents.PolGrad
import os
from threading import Thread

LINE_SEPARATOR = '\n'
BUF_SIZE = 4096 #in bytes

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
def connect(host, port):
	try:
		sock.connect((host, port))
	except socket.error:
		print 'Unable to contact environment at the given host/port.'
		sys.exit(1)
        
def sendStr(s):
    sock.send(s + LINE_SEPARATOR)
    
def receive(numTokens):
    data = ['']
    while len(data) <= numTokens:
        rawData = data[-1] + sock.recv(BUF_SIZE)
        del data[-1]
        data = data + rawData.split(LINE_SEPARATOR)
        
    del data[-1]
    return data
    
def sendAction(action):
    #sends all the components of the action one by one
    for a in action:
        sendStr(str(a))

#main procedure that handles the protocol

#initialize the necessary parameters for runing the experiment
host = 'localhost'
port = 8888 #default port number 
numEpisodes = 1 #manually change it for different experiments 
agentParams = 1 #not used for the random agent

connect(host, port)

sendStr('GET_TASK')
data = receive(2)
stateDim = int(data[0])
actionDim = int(data[1])
print 'The state space is %d dimensional and the action space is %d dimensional' % (stateDim, actionDim)

#instantiate agent
agent = agents.RandomAgent.Agent.Agent(stateDim, actionDim, agentParams)
#agentParams = array([.002, .1, .3, .3, 0.0, 0.1, 10])
#agent = agents.PolGrad.TD_PolGrad(stateDim, actionDim, agentParams)
#agentParams = array([.1, .01, .01, .9, 0.5])
#agent = agents.PolBack.TD_PolGrad(stateDim, actionDim, agentParams)

sendStr('START_LOG')
sendStr(agent.getName())
print 'The agent named %s has been initialized...' % agent.getName()

for i in range(numEpisodes):
    the_counter = 0
    sendStr('START')
    data = receive(2+stateDim)
    
    terminalFlag = int(data[0])
    state = map(float, data[2:])
    action = agent.start(state)
    
    while not terminalFlag:
        the_counter += 1
        sendStr('STEP')
        sendStr(str(actionDim))
        sendAction(action)
        
        data = receive(3 + stateDim)
        if not(len(data) == stateDim + 3):
            print 'Communication error: calling agent.cleanup()'
            agent.cleanup()
            sys.exit(1)
            
        reward = float(data[0])
        terminalFlag = int(data[1])
        state = map(float, data[3:])
        print 'episode %d: reward = %f, the_counter= %d' % (i, reward, the_counter)
        
        action = agent.step(reward, state)

 
