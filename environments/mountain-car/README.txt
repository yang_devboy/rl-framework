Mountain car

This implementation is based on Mountain Car with Sensorimotor Delay, http://www.cs.mcgill.ca/~dprecup/workshops/ICML06/mountaincar.html

There are two continuous state variables: x1 (the position of the car) and x2 (the velocity of the car). There are three discrete actions: 2 (forward throttle), 1 (no throttle), 0 (backward throttle). The starting state of the problem is in the region: −1.1 ≤ x1 < 0.5, x2 = 0. Goal states are those with x1 ≥ 0.5. Finally, the reward is −1 at each step and 0 if a goal state is reached.