Puddle world

This implementation is based on Sutton, R.S. (1996). Generalization in reinforcement learning: Successful examples using sparse coarse coding, NIPS 1996.

The objective in this task is to drive a point robot in a continuous rectangular 2-dimensional terrain to a goal area (unknown to the robot) located at the upper right corner, while avoiding the two puddle areas. The 2-dimensional area is defined as [0, 1]2. The puddles are oval in shape with a radius of 0.1 and are located at center points (.1, .75) to (.45, .75) and (.45, .4) to (.45, .8).

There are two continuous state variables: the position (x,y) of the robot. There are four discrete actions: 2 - up, 3 - down, 0 - right, and 1 - left. Each action moves the robot by 0.05 in the corresponding direction, up to the limits of the area. A random gaussian noise with mean = 0 and std = 0.01 is also added to the motion along each dimension. Starting states are chosen uniformly over [0, 0.95)2 and the goal states are those with x ≥ 0.95 and y ≥ 0.95. Finally, there is negative reward (i.e., -1) at each time step plus additional penalties if either or both
of the two oval “puddles” are entered. These penalties are 400 times the distance inside the puddle.