import protocol.*;
import protocol.Observable; /* disambiguate with java.util.Observable */

import javax.xml.bind.JAXBElement;
import config.*;
import environment.*;
import ODEFramework.*;

import java.util.*;

public class Environment {

    private Arm arm;
    private Set<Target> targets;
    private Set<Food> food;
    private Mouth mouth;

    private TaskTracker taskTracker;

    private EnvironmentSimulator envSimulator;
    private ODESolver solver;
    private ODEState initialState;

    private List<EnvironmentObserver> observers;

    public Environment(EnvSpec spec) {
        arm = new Arm(spec.getArm());
        targets = new HashSet<Target>();
        food = new HashSet<Food>();
        mouth = null;

        taskTracker = makeTaskTracker(spec.getTask().getValue());
        envSimulator = new EnvironmentSimulator(arm, food);

        initialState = envSimulator.getCurrentODEState();
        solver = new RungeKutta4Solver();

        observers = new ArrayList<EnvironmentObserver>();
    }

    public TaskSpec getTaskSpec() {
        int numCompartments = arm.getCompartments().size();
        int numFood = food.size();
        int numStateVars = 2 + 4 * (2 * numCompartments + numFood);
        int numActionVars = 3 * numCompartments;
        return new TaskSpec(numStateVars, numActionVars);
    }

    public State start() {
        taskTracker.reset();
        envSimulator.setODEState(0.0, initialState);
        State state = new State(initialState.getArray(), false);

        for (EnvironmentObserver o: observers) {
            o.episodeStarted();
        }
        return state;
    }

    public Observable step(double[] action) {
        if (taskTracker.isTerminal()) {
            State rlState = new State(
                envSimulator.getCurrentODEState().getArray(),
                true);
            return new Observable(rlState, 0.0);
        }

        List<Compartment> compartments = arm.getCompartments();

        double closestDistance = 0;
        double armEndDistance = 0;

        int targetIndex = 1, compartmentIndex = -1;
        double minDistance = Double.MAX_VALUE;
        double distance;
        for (Target t : targets) {
        	for (int i = 0; i < compartments.size(); i++) {
        		distance = compartments.get(i).getMidpointDistance(t.getPosition());
        		if (distance < minDistance) {
        			minDistance = distance;
        			compartmentIndex = i;
        		}
        	}

        	closestDistance += compartments.get(compartmentIndex).getNodeDistance(t.getPosition()) + minDistance;
        	armEndDistance += compartments.get(compartments.size()-1).getNodeDistance(t.getPosition()) + compartments.get(compartments.size()-1).getMidpointDistance(t.getPosition());

        	System.out.println("For Target " + targetIndex + "(previous state):\n");
        	System.out.println("The distance of the closest compartment to the target: " + compartments.get(compartmentIndex).getNodeDistance(t.getPosition()) + " & " + minDistance);
        	System.out.println("The distance of the end of the arm to the target: " + compartments.get(compartments.size()-1).getNodeDistance(t.getPosition()) + " & " + compartments.get(compartments.size()-1).getMidpointDistance(t.getPosition()));

        	targetIndex++;
        	minDistance = Double.MAX_VALUE;
        }

        /* guard against oversized or undersized action arrays */
        int usedCompartments = Math.min(action.length / 3, compartments.size());
        for (int i = 0; i < usedCompartments; i++) {
            compartments.get(i).setAction(action[3*i], action[3*i+1], action[3*i+2]);
        }

        ODEState odeState = envSimulator.getCurrentODEState();
        odeState = solver.solve(envSimulator, odeState, 0, 5, .2);
        envSimulator.setODEState(.1, odeState);

        taskTracker.update();

        boolean terminal = taskTracker.isTerminal();
        State rlState = new State(
                envSimulator.getCurrentODEState().getArray(), terminal);

        double newClosestDistance = 0;
        double newArmEndDistance = 0;

        compartments = arm.getCompartments();
        targetIndex = 1;
        compartmentIndex = -1;
        minDistance = Double.MAX_VALUE;
        for (Target t : targets) {
        	for (int i = 0; i < compartments.size(); i++) {
        		distance = compartments.get(i).getMidpointDistance(t.getPosition());
        		if (distance < minDistance) {
        			minDistance = distance;
        			compartmentIndex = i;
        		}
        	}

        	newClosestDistance += compartments.get(compartmentIndex).getNodeDistance(t.getPosition()) + minDistance;
        	newArmEndDistance += compartments.get(compartments.size()-1).getNodeDistance(t.getPosition()) + compartments.get(compartments.size()-1).getMidpointDistance(t.getPosition());

        	System.out.println("For Target " + targetIndex + "(updated state):\n");
        	System.out.println("The distance of the closest compartment to the target: " + compartments.get(compartmentIndex).getNodeDistance(t.getPosition()) + " & " + minDistance);
        	System.out.println("The distance of the end of the arm to the target: " + compartments.get(compartments.size()-1).getNodeDistance(t.getPosition()) + " & " + compartments.get(compartments.size()-1).getMidpointDistance(t.getPosition()));

        	targetIndex++;
        	minDistance = Double.MAX_VALUE;
        }

        // 0 denotes using the nearest-compartment distance, otherwise uses the end-of-arm distance
        double reward = taskTracker.getReward();
        if (reward == 0) {
        	reward = newClosestDistance - closestDistance;
        } else if (reward == 1) {
        	reward = newArmEndDistance - armEndDistance;
        } else {
        	// do nothing as we hit one of the targets
        }

        Observable observable = new Observable(rlState, reward);

        for (EnvironmentObserver o: observers) {
            o.stateChanged(reward);
            if (terminal) {
                o.episodeFinished();
            }
        }
        return observable;
    }


    /* accessors for parts of the environment (needed for display) */

    public Arm getArm() {
        return arm;
    }

    public Set<Food> getFood() {
        return Collections.unmodifiableSet(food);
    }

    public Mouth getMouth() {
        return mouth;
    }

    public Set<Target> getTargets() {
        return Collections.unmodifiableSet(targets);
    }

    /* observation support (needed for display) */

    public void addObserver(EnvironmentObserver o) {
        observers.add(o);
    }

    /* TaskTracker class hierarchy  */

    private TaskTracker makeTaskTracker(TaskDef def) {
        /* can't use polymorphism here, since TaskDef subclasses are schema-
         * generated and shouldn't be modified (modifications are lost on
         * regeneration) */
        if (def instanceof FoodTaskDef) {
            return new FoodTaskTracker((FoodTaskDef) def);
        } else if (def instanceof TargetTaskDef) {
            return new TargetTaskTracker((TargetTaskDef) def);
        } else {
            throw new IllegalArgumentException("Unknown task definition given.");
        }
    }

    private interface TaskTracker {
        public void reset();
        public void update();
        public boolean isTerminal();
        public double getReward();
    }

    private abstract class TimeLimitTaskTracker implements TaskTracker {

        private int timeLimit;
        private double stepReward;

        private int timeLeft;

        protected TimeLimitTaskTracker(TaskDef def) {
            this.timeLimit = def.getTimeLimit().intValue();
            this.stepReward = def.getStepReward();
            timeLeft = timeLimit;
        }

        public void reset() {
            timeLeft = timeLimit;
        }

        public void update() {
            timeLeft--;
        }

        public boolean isTerminal() {
            return (timeLeft == 0);
        }

        public double getReward() {
            return stepReward;
        }
    }

    private class FoodTaskTracker extends TimeLimitTaskTracker {

        private boolean subgoalAchieved;
        private double reward;
        private Set<Food> initialFood;

        public FoodTaskTracker(FoodTaskDef taskDef) {
            super(taskDef);
            mouth = new Mouth(taskDef.getMouth());

            initialFood = new HashSet<Food>();
            for (FoodSpec fs: taskDef.getFood()) {
                initialFood.add(new Food(fs));
            }
            initialFood = Collections.unmodifiableSet(initialFood);

            food.addAll(initialFood);
        }

        public void reset() {
            super.reset();
            food.addAll(initialFood);
        }

        public void update() {
            super.update();
            subgoalAchieved = false;
            reward = 0.0;
            for (Iterator<Food> i = food.iterator(); i.hasNext(); ) {
                Food f = i.next();
                if (mouth.getShape().contains(f.getPosition().toPoint2D())) {
                    i.remove();
                    f.warp();
                    subgoalAchieved = true;
                    reward += f.getValue();
                }
            }
        }

        public boolean isTerminal() {
            return (super.isTerminal() || food.isEmpty());
        }

        public double getReward() {
            return subgoalAchieved ? reward : super.getReward();
        }
    }

    private class TargetTaskTracker extends TimeLimitTaskTracker {

        private ObjectiveTracker objectiveTracker;
        private boolean subgoalAchieved;
        private double reward;

        public TargetTaskTracker(TargetTaskDef def) {
            super(def);
            objectiveTracker = makeObjectiveTracker(def.getObjective().getValue());
        }

        public void reset() {
            super.reset();
            objectiveTracker.reset();
            objectiveTracker.makeEligible();
        }

        public void update() {
            super.update();
            subgoalAchieved = objectiveTracker.check();
        }

        public double getReward() {
            return subgoalAchieved ? reward : super.getReward();
        }

        public boolean isTerminal() {
            return (super.isTerminal() || objectiveTracker.isAccomplished());
        }

        private ObjectiveTracker makeObjectiveTracker(ObjectiveSpec spec) {
            /* can't use polymorphism here, since ObjectiveSpec and subclasses
             * are schema-generated */
             if (spec instanceof SequenceSpec) {
                 return new SequenceTracker((SequenceSpec) spec);
             } else if (spec instanceof ChoiceSpec) {
                 return new ChoiceTracker((ChoiceSpec) spec);
             } else if (spec instanceof TargetSpec) {
                 return new SingleTargetTracker((TargetSpec) spec);
             } else {
                 throw new IllegalArgumentException("Unknown objective type.");
             }
        }

        private abstract class ObjectiveTracker {
            public abstract void reset();
            public abstract boolean check();
            public abstract boolean isAccomplished();

            /* for display purposes */
            public abstract void makeEligible();
            public abstract void makeIneligible();
        }

        private class SequenceTracker extends ObjectiveTracker {

            private List<ObjectiveTracker> subObjectives;
            private int current;
            private boolean accomplished;

            public SequenceTracker(SequenceSpec spec) {
                subObjectives = new ArrayList<ObjectiveTracker>();
                for (JAXBElement<? extends ObjectiveSpec> os: spec.getObjective()) {
                    subObjectives.add(makeObjectiveTracker(os.getValue()));
                }
                current = 0;
                accomplished = false;
            }

            public void reset() {
                for (ObjectiveTracker o: subObjectives) {
                    o.reset();
                }
                current = 0;
                accomplished = false;
            }

            public boolean check() {
                boolean hit = subObjectives.get(current).check();
                if (hit && subObjectives.get(current).isAccomplished()) {
                    subObjectives.get(current).makeIneligible();
                    current++;

                    if (current < subObjectives.size()) {
                        subObjectives.get(current).makeEligible();
                    } else {
                        accomplished = true;
                    }
                }
                return hit;
            }

            public boolean isAccomplished() {
                return accomplished;
            }

            public void makeEligible() {
                subObjectives.get(0).makeEligible();
                for (int i = 1; i < subObjectives.size(); i++) {
                    subObjectives.get(i).makeIneligible();
                }
            }

            public void makeIneligible() {
                for (ObjectiveTracker o: subObjectives) {
                    o.makeIneligible();
                }
            }
        }

        private class ChoiceTracker extends ObjectiveTracker {
            private Set<ObjectiveTracker> subObjectives;
            private ObjectiveTracker selected;
            private boolean accomplished;

            public ChoiceTracker(ChoiceSpec spec) {
                subObjectives = new HashSet<ObjectiveTracker>();
                for (JAXBElement<? extends ObjectiveSpec> os: spec.getObjective()) {
                    subObjectives.add(makeObjectiveTracker(os.getValue()));
                }
                selected = null;
                accomplished = false;
            }

            public void reset() {
                for (ObjectiveTracker o: subObjectives) {
                    o.reset();
                }
                selected = null;
                accomplished = false;
            }

            public boolean check() {
                boolean hit = false;
                if (selected == null) {
                    for (ObjectiveTracker o: subObjectives) {
                        hit = o.check();
                        if (hit) {
                            selected = o;
                            break;
                        }
                    }

                    if (hit) {
                        for (ObjectiveTracker o: subObjectives) {
                            if (o != selected) {
                                o.makeIneligible();
                            }
                        }
                    }
                } else {
                    hit = selected.check();
                }

                if (selected != null) {
                    accomplished = selected.isAccomplished();
                    if (accomplished) {
                        selected.makeIneligible();
                    }
                }
                return hit;
            }

            public boolean isAccomplished() {
                return accomplished;
            }

            public void makeEligible() {
                for (ObjectiveTracker o: subObjectives) {
                    o.makeEligible();
                }
            }

            public void makeIneligible() {
                for (ObjectiveTracker o: subObjectives) {
                    o.makeIneligible();
                }
            }
        }

        private class SingleTargetTracker extends ObjectiveTracker {

            private Target target;
            private boolean accomplished;

            public SingleTargetTracker(TargetSpec spec) {
                target = new Target(spec);
                targets.add(target);
                accomplished = false;
            }

            public void reset() {
                accomplished = false;
            }

            public boolean check() {
                boolean hit = false;
                for (Compartment c: arm.getCompartments()) {
                    if (c.getShape().contains(target.getPosition().toPoint2D())) {
                        hit = true;
                        reward = target.getValue();
                        break;
                    }
                }

                accomplished = hit;
                return hit;
            }

            public boolean isAccomplished() {
                return accomplished;
            }

            public void makeEligible() {
                target.setEligible(true);
            }

            public void makeIneligible() {
                target.setEligible(false);
            }
        }
    }
}
