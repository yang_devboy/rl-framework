# Echo client program
import socket
import sys
from numpy import * #import random
#import agents.RandomAgent.Agent
# import agents.PolBack
# import agents.PolGrad
import Agent
import os
from threading import Thread

LINE_SEPARATOR = '\n'
BUF_SIZE = 4096 #in bytes

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

envpath = 'H:/Works/OneDrive/PhD/Writing/GitRepo/rl-framework/environments'
# envpath = 'H:/Works/OneDrive/PhD/Writing/GitRepo/rl-framework/environments'
# envpath = 'H:/Works/OneDrive/PhD/Writing/GitRepo/rl-framework/environments'
# envpath = '/am/phoenix/home1/gif/Desktop/PhD/Writing/GitRepo/fishertca/rl-framework/environments' 

xmlconf = 'settings.xml'
xmlstate = 'initialStates.xml'
mode = 'internal' # 'external_gui', 'external', 'internal'
        
def connect(host, port):
	try:
		sock.connect((host, port))
	except socket.error:
		print 'Unable to contact environment at the given host/port.'
		sys.exit(1)
        
def sendStr(s):
    sock.send(s + LINE_SEPARATOR)
    
def receive(numTokens):
    data = ['']
    while len(data) <= numTokens:
        rawData = data[-1] + sock.recv(BUF_SIZE)
        del data[-1]
        data = data + rawData.split(LINE_SEPARATOR)
        
    del data[-1]
    return data
    
def sendAction(action):
    #sends all the components of the action one by one
    for a in action:
        sendStr(str(a))
        
def init_octopus_rotating_env():
    os.system('java -Djava.endorsed.dirs='+envpath+'/octopus-arm-rotating/lib -jar '+envpath+'/octopus-arm-rotating/octopus-environment.jar external_gui '+envpath+'/octopus-arm-rotating/'+xmlconf+' '+envpath+'/octopus-arm-rotating/'+xmlstate+' 8888')

def initEnvironment():
    os.system('java -Djava.endorsed.dirs=./environments/octopus-arm/lib -jar ./environments/octopus-arm/octopus-environment.jar external ./environments/octopus-arm/settings.xml 8888')

def initEnvironment2():
    os.system('java -jar ./environments/mountain-car/mountaincar-environment.jar random 8888') #start with a random car position
    #os.system('java -jar ./environments/mountain-car/mountaincar-environment.jar fixed -0.5 8888') #start with a specific car position

def initEnvironment3():
    os.system('java -jar ./environments/puddle-world/puddleworld-environment.jar random 8888') #start with a random position
    #os.system('java -jar ./environments/puddle-world/puddleworld-environment.jar fixed 0.5 0.2 8888') #start with a specific position

#main procedure that handles the protocol
environment = 0
while environment <= 0:
    environment = int(raw_input('Please enter the index of one of the following environments:\n1. Control an octopus arm\n2. Mountain Car\n3. Puddle World\n'))

if environment == 1:
    # thread = Thread(target = initEnvironment, args = [])
    thread = Thread(target = init_octopus_rotating_env, args = [])
    print 'Please wait while initializing the environment'
    thread.start()
    thread.join(2)
    print 'The environment has been initialized...'
elif environment == 2:
    thread = Thread(target = initEnvironment2, args = [])
    print 'Please wait while initializing the environment'
    thread.start()
    thread.join(2)
    print 'The environment has been initialized...'
elif environment == 3:
    thread = Thread(target = initEnvironment3, args = [])
    print 'Please wait while initializing the environment'
    thread.start()
    thread.join(2)
    print 'The environment has been initialized...'
else:
    print 'Unsupported environment and the program will exit...'
    sys.exit(1)

#initialize the necessary parameters for runing the experiment
host = 'localhost'
port = 8888 #default port number 
numEpisodes = int(raw_input('Please enter the number of episodes: '))
agentParams = 1 #not used for the random agent

connect(host=host, port=port)

sendStr('GET_TASK')
data = receive(2)
stateDim = int(data[0])
actionDim = int(data[1])
print 'The state space is %d dimensional and the action space is %d dimensional' % (stateDim, actionDim)

#instantiate agent
#agent = agents.RandomAgent.Agent.Agent(stateDim, actionDim, agentParams)
# agentParams = array([.002, .1, .3, .3, 0.0, 0.1, 10])
# agent = agents.PolGrad.TD_PolGrad(stateDim, actionDim, agentParams)
#agentParams = array([.1, .01, .01, .9, 0.5])
#agent = agents.PolBack.TD_PolGrad(stateDim, actionDim, agentParams)

agent = Agent.Agent(stateDim, actionDim, agentParams) #random agent

# sendStr('START_LOG')
# sendStr(agent.getName())
print 'The agent named %s has been initialized...' % agent.getName()

for i in range(numEpisodes):
    the_counter = 0
    sendStr('START')
    data = receive(2+stateDim)
    
    terminalFlag = int(data[0])
    state = map(float, data[2:])
    print 'state: ',state
    print 'data : ',datas

    action = agent.start(state)
    
    while not terminalFlag:
        the_counter += 1
        sendStr('STEP')
        sendStr(str(actionDim))
        sendAction(action)
        
        data = receive(3 + stateDim)
        if not(len(data) == stateDim + 3):
            print 'Communication error: calling agent.cleanup()'
            agent.cleanup()
            sys.exit(1)
            
        reward = float(data[0])
        terminalFlag = int(data[1])
        state = map(float, data[3:])
        print 'episode %d: reward = %f, the_counter= %d' % (i, reward, the_counter)
        
        action = agent.step(reward, state)
        print 'agent :',action

